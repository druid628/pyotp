# pyOTP #
With a cheap rtl-sdr and [rtl-entropy](https://github.com/pwarren/rtl-entropy) I found myself with lots of random data.  These scripts are an attempt to put that random data to use.
### OTP? ###
[One-time pad](http://en.wikipedia.org/wiki/One-time_pad) (OTP) encryption is unbreakable when used correctly.  The plain text is combined with a random key (pad).  The key has to be at least as long as the plain text.  The key is destroyed after one use and the recipient must already have a copy.
### How do I get set up? ###
* Make 2 directories (input and output).
* Create some one-time pads in each directory:
```bash
for i in `seq 1 100`;do
  echo makepad $i
  makepad.sh
  sleep $[RANDOM%10]
done
```
* Zip up the folders and swap their names (input <-> output).
* You will have to find a way to securely transfer the zip file to your recipient.
### Encrypt ###
```sh
otp.py encrypt message.txt message.bin path/to/output_key_folder
```
A message will be displayed as each used one-time pad is deleted.  The original file to be encrypted is not deleted.
### Decrypt ###
```sh
otp.py decrypt message.bin message.txt path/to/input_key_folder
```
A message will be displayed as each used one-time pad is deleted.  The encrypted file is not deleted although you no longer have the files necessary for decryption.
### How does it work? ###
#### Preparation ####
2 folders need to be filled with files with random names containing random data.  makepad.sh is a good starting point for this.  These are the one-time pads.  They must be kept secret and they must be deleted after one use.  One folder will be used for incoming messages and one for outgoing messages.  It might be a good idea to encrypt these files or keep them on a USB stick or both!

If you will be communicating with multiple recipients you will need an incoming and outgoing folder for each.
#### Encryption ####
During encryption a random file from the key folder will be selected.  The file name of the one-time pad used is saved to the output file.  It will be used during decryption to determine which pad to use.

The entire pad is loaded into ram at once and deleted.  The default pad size that makepad.sh creates is around 1k.  The sizes vary slightly to make it harder to determine what part of the output file is encrypted data and what part is the name of the next pad.  This only matters if the file is larger than the first pad.

The input file is XORed with the key file byte at a time.  If the file to be encrypted is larger than the pad more than one pad will be used.  Each pad will be deleted after it is read.

If there are not enough pads to encrypt the file the process will not be started.
#### Decryption ####
During decryption the length of the pad file names will be determined by the length of the name of the first file in the key folder.  This means that you should not mix pads with different length names in the same folder.

Once the file name length has been determined that many bytes of the encrypted file will be read.  The key file will be loaded from the key folder and deleted.

The encrypted file will be XORed with the key file returning the original file.  If the encrypted file is larger than the first key file the next pad name will be pulled from the encrypted file and the process repeated until the entire file is decrypted.
### What do the scripts do? ###
#### makepad.sh ####
This script will make 1 key file (pad) for you.  By default the file will be around 1k in size with the random data coming from /var/run/rtl_entropy.fifo.  This can be changed to another random source but it might take longer.
#### otp.py ####
This script does the actual encryption and decryption.  For encryption it needs the file to encrypt, a name to call the encrypted file and a directory with enough keys to encrypt the entire file.  For decryption it needs the file to decrypt, a name to call the decrypted file and a directory that contains the same keys that were used to encrypt the file.  In both cases the key files are deleted as they are used.
#### padtodigits.py ####
This script will convert a key file into random digits or letters.  These digits/letters can be used to [encode or decode something by hand](http://users.telenet.be/d.rijmenants/en/onetimepad.htm).  There are two ways to run this script:
```sh
padtodigits.py path/to/key_file
```
or
```sh
padtodigits.py path/to/key_file letters
```
The key file will **NOT** be deleted automatically.  If you use a key file for encryption be sure to [securely](http://man7.org/linux/man-pages/man1/shred.1.html) delete it.
#### splitpad.py ####
This script will split a large file of random data into smaller pieces for use by the otp.py script.  If you have a file of random data on a USB stick or burned to a DVD this will split it in a deterministic way.  If 2 people run this on the same data they will both get the same key files.  Remember to delete the file after it has been split.

Why not just use the [split](http://man7.org/linux/man-pages/man1/split.1.html) command?  This script will gather the random key names, random lengths, and the contents for the keys from the random data.  With the same input file and an empty folder it will generate the same keys each time.  Using split each key will be the same size so there is a clear delineation between data and metadata (bad).  You will also have to come up with a method for naming the keys.  otp.py expects the key names to be an even number of random hex digits.
## Keeping your keys secure ##
The otp.py script tries to keep your keys secure by overwriting and deleting them as soon as they are used.  This may not be sufficient if you are using a filesystem that uses copy-on-write, snapshots, automated backups, compression, or flash memory.  The best option is to store the keys in an encrypted folder, partition or drive.  It will be much easier to securely remove the keys if they were never written to the drive unencrypted.
## Message protocol ##
To some extent you will have to come up with your own protocols that match your situation.  You will have to decide how to get your keys and messages to your recipient.  That said, here are some suggestions:

* Send the input and output folders as you expect them to be used.  Do not expect the recipient to swap the names before use.
* Do not use the Internet (or any computer network) to distribute the keys.
* Compress the file before encrypting.  The less data an eavesdropper has to work with the better.
* Consider encrypting the encrypted file with something more mundane (public key encryption or encrypted 7z, rar, or zip) so that an eavesdropper does not know that one-time pad is being used.