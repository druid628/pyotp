#!/bin/sh

DEV=/var/run/rtl_entropy.fifo
SIZE=1024
NAMEBYTES=4

NAME=$(xxd -l $NAMEBYTES -p "$DEV")
RAND=$(tr -cd 0-$NAMEBYTES < "$DEV" | head -c 1)
LENGTH=$(( SIZE-RAND ))

dd if="$DEV" of="$NAME" bs=$LENGTH count=1 2>/dev/null
