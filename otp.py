#!/usr/bin/env python

# One-time pad
# Jack Zielke

# Faster xor from Omnifarious
# http://stackoverflow.com/a/5738701

from binascii import hexlify as _hexlify
from binascii import unhexlify as _unhexlify

import os
import random
import sys
import struct

def usage():
  print 'Usage:'
  print '	' + sys.argv[0] + ' [-t] <encrypt|decrypt> <infile> <outfile> <key directory>'
  print '	-t = testing, do not delete used key files'
  sys.exit(1)

def check_files(infile, keydir):
  if not os.path.isfile(infile):
    print 'No such file', infile
    return False
  elif not os.path.isdir(keydir):
    print 'No such directory', keydir
    return False
  return True

def check_keyspace(infile, keydir):
  need = os.path.getsize(infile)
  have = 0
  files = os.listdir(keydir)
  for file in files:
    have += os.path.getsize(keydir + os.sep + file)
    if have >= need:
      return True
  print 'Not enough key files in', keydir, 'to encrypt'
  return False

def xor(data, key):
  dlen    = len(data)
  key     = key[:dlen]
  data    = int(_hexlify(data), 16)
  key     = int(_hexlify(key), 16)
  encoded = (data ^ key) | (1 << (dlen * 8 + 7))
  encoded = buffer(hex(encoded))
  encoded = (encoded[4:-1] if encoded[-1] == 'L' else encoded[4:])
  encoded = _unhexlify(encoded)
  return encoded

def delete(file):
  print 'deleting', os.path.basename(file)
  size = os.path.getsize(file)
  d = open(file, 'wb', 0)
  d.write(''.ljust(size, chr(255)))
  d.flush()
  os.fsync(d.fileno())
  d.seek(0, 0)
  d.write(''.ljust(size, chr(0)))
  d.flush()
  os.fsync(d.fileno())
  for i in range(5):
    d.seek(0, 0)
    d.write(os.urandom(size))
    d.flush()
    os.fsync(d.fileno())
  d.truncate(0)
  d.close()
  tmpfile = os.path.dirname(file) + '/.' + str(_hexlify(os.urandom(4)))
  try:
    os.rename(file, tmpfile)
    os.remove(tmpfile)
  except:
    os.remove(file)

def encrypt(infile, outfile, keydir, testing):
  if check_files(infile, keydir) and check_keyspace(infile, keydir):
    i = open(infile, 'rb')
    o = open(outfile, 'wb')
    os.chmod(outfile, 0o600)
    files = os.listdir(keydir)
    key = False
    while 1:
      while not key:
        keyfile = random.choice(files)
        files.remove(keyfile)
        k = open(keydir + os.sep + keyfile, 'rb')
        key = k.read(-1)
        k.close()
        if not testing:
          delete(keydir + os.sep + keyfile)
        else:
          print 'using', keyfile
        o.write(_unhexlify(keyfile))
      plain = i.read(len(key))
      if not plain:
        break
      o.write(xor(plain, key))
      if i.read(1):
        i.seek(-1,1)
        key = False
    i.close()
    o.close()
  else:
    sys.exit(1)

def decrypt(infile, outfile, keydir, testing):
  if check_files(infile, keydir):
    files = os.listdir(keydir)
    if len(files) == 0:
      print keydir, 'is empty'
      sys.exit(1)
    idBytes = len(files[0])/2
    i = open(infile, 'rb')
    o = open(outfile, 'wb')
    os.chmod(outfile, 0o600)
    finished = False
    while not finished:
      binkey = i.read(idBytes)
      keyfile = _hexlify(binkey)
      k = open(keydir + os.sep + keyfile, 'rb')
      key = k.read(-1)
      k.close()
      if not testing:
        delete(keydir + os.sep + keyfile)
      else:
        print 'using', keyfile
      ciphered = i.read(len(key))
      if not ciphered:
        break
      o.write(xor(ciphered, key))
      if i.read(1):
        i.seek(-1,1)
      else:
        finished = True
    i.close()
    o.close()
  else:
    sys.exit(1)

if __name__=='__main__':
  if len(sys.argv) > 4 and len(sys.argv) < 7:
    count = 0
    testing = False
    for arg in sys.argv[1:]:
      if arg == '-t':
        testing = True
        count -= 1
      elif count == 0:
        command = arg
      elif count == 1:
        infile = arg
      elif count == 2:
        outfile = arg
      elif count == 3:
        keydir = arg.rstrip(os.sep)
      count += 1
    if command == 'encrypt':
      encrypt(infile, outfile, keydir, testing)
    elif command == 'decrypt':
      decrypt(infile, outfile, keydir, testing)
    else:
      print 'unknown command', command
      usage()
  else:
    usage()