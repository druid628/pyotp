#!/usr/bin/env python

import os
import sys
from math import log
from binascii import hexlify as _hexlify
from binascii import unhexlify as _unhexlify

cols = 13
group = 5

letters = False
recoverbin = ''
recoverdata = ''

def usage():
  print 'Usage:'
  print '	' + sys.argv[0] + ' <infile> [letters]'
  print '	letters option prints letters instead of digits'
  sys.exit(1)

def check_files(infile):
  if not os.path.isfile(infile):
    print 'No such file', infile
    return False
  return True

def chartodigit(char):
  # http://security.stackexchange.com/a/39643
  # Andreas Krey
  global recoverbin
  seed = int(_hexlify(char), 16)
  max = str(256**len(char)-1)
  count = len(max)
  for i in range(1, count):
    div = int(max[:i].ljust(count, '0'))
    if seed < div:
      return str(seed).zfill(count-i)[-(count-i):]
  recoverbin += bitrecovery(seed % 10)
  return ''

def chartoletter(char):
  # https://groups.google.com/forum/#!msg/sci.crypt.random-numbers/tI0P321CraQ/HsY-t3w4sjYJ
  # Ilmari Karonen
  letters = ''
  num = int(_hexlify(char), 16)
  if len(char) == 4:
    if num < 0xef5dd940:
      for i in range(6):
        letters += chr(num%26+65)
        num //= 26
    else:
      # >= 0xef5dd940
      if num < 0xff5dd940:
        letters = chartoletter(char[1:])
      elif num < 0xffffd940:
        letters = chartoletter(char[2:])
      elif num < 0xffffff40:
        letters = chartoletter(char[3])
      else:
        letterrecovery2(hex(num)[-2:])
  else:
    for i in range(len(char)):
      num = ord(char[i])
      if num < 234:
        letters += chr(num%26+65)
      else:
        letterrecovery(num)
  return letters

def bitrecovery(digit):
  if digit == 0:
    return '10'
  if digit == 1:
    return '11'
  if digit == 2:
    return '00'
  if digit == 3:
    return '01'
  if digit == 4:
    return '0'
  if digit == 5:
    return '1'
  return ''

def letterrecovery(num):
  global recoverbin
  if num > 253:
    start = -1
  elif num > 249:
    start = -2
  elif num > 233:
    start = -4
  else:
    return
  recoverbin += bin(num)[start:]

def letterrecovery2(hex):
  global recoverbin
  num = int(hex, 16)
  if num < 64:
    return
  if num > 251:
    start = -1
  else:
    if num < 128:
      num += 64
    start = int(log(255-num, 2))*-1
  recoverbin += bin(num)[start:]

def padtodigits(infile):
  global cols, group, letters, recoverbin, recoverdata, totallen, sequence
  row = ''
  printme = ''
  counter = 0
  if letters:
    bytes = 4
  else:
    bytes = 49
  if check_files(infile):
    basename = os.path.basename(infile)
    try:
      name = str(int(basename, 16))
      max = len(str(int(16.**len(basename)-1)))
      name = name.zfill(max)
      if letters:
        name += ', ' + chartoletter(_unhexlify(basename))
      name = infile + ', ' + name
    except:
      name = infile
    print 'File:', name
    i = open(infile, 'rb')
    char = i.read(bytes)
    while char != '':
      if letters:
        printme += chartoletter(char)
        while len(recoverbin) >= 8:
          recoverdata += chr(int(recoverbin[:8], 2))
          recoverbin = recoverbin[8:]
        while len(recoverdata) >= 4:
          printme += chartoletter(recoverdata[:4])
          recoverdata = recoverdata[4:]
        if len(recoverdata) > 0 and len(char) < bytes:
          printme += chartoletter(recoverdata)
      else:
        printme += chartodigit(char)
        while len(recoverbin) >= 8:
          recoverdata += chr(int(recoverbin[:8], 2))
          recoverbin = recoverbin[8:]
        while len(recoverdata) >= 49:
          printme += chartodigit(recoverdata[:49])
          recoverdata = recoverdata[49:]
        if len(recoverdata) > 0 and len(char) < bytes:
          printme += chartodigit(recoverdata)
      while len(printme) >= group:
        row += printme[:5] + ' '
        counter += 1
        printme = printme[5:]
        if counter == cols:
          print row
          row = ''
          counter = 0
      char = i.read(bytes)
    i.close()
    if counter > 0:
      print row
  else:
    sys.exit(1)

if __name__=='__main__':
  if len(sys.argv) > 1:
    infile = sys.argv[1]
    for arg in sys.argv[2:]:
      if arg == 'letters':
        letters = True
    padtodigits(infile)
  else:
    usage()